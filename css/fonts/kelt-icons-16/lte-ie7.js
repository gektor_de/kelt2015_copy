/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'kelt-icons-16\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon16-bullhorn' : '&#xe000;',
			'icon16-podcast' : '&#xe001;',
			'icon16-calendar' : '&#xe003;',
			'icon16-bubble' : '&#xe004;',
			'icon16-lab' : '&#xe009;',
			'icon16-lanyrd' : '&#xe00b;',
			'icon16-twitter' : '&#xe00c;',
			'icon16-film' : '&#xe00e;',
			'icon16-location' : '&#xe00f;',
			'icon16-mail' : '&#xe013;',
			'icon16-microphone' : '&#xe014;',
			'icon16-graduate' : '&#xe016;',
			'icon16-coffee' : '&#xf0f4;',
			'icon16-attachment' : '&#xe01a;',
			'icon16-hammer' : '&#xf291;',
			'icon16-food' : '&#xe007;',
			'icon16-picture' : '&#xf03e;',
			'icon16-user' : '&#xe008;',
			'icon16-clock' : '&#xe002;',
			'icon16-bubbles' : '&#xe010;',
			'icon16-house' : '&#xe005;',
			'icon16-ticket' : '&#xe011;',
			'icon16-uniF47D' : '&#xf47d;',
			'icon16-tag' : '&#xf02b;',
			'icon16-pencil' : '&#xf040;',
			'icon16-tv' : '&#xe006;',
			'icon16-play-alt' : '&#xe00a;',
			'icon16-file-pdf' : '&#xe00d;',
			'icon16-attachment-2' : '&#xe012;',
			'icon16-presentation' : '&#xf0c4;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon16-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};