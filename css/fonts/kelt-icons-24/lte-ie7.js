/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'kelt-icons-24\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon24-bullhorn' : '&#xe000;',
			'icon24-podcast' : '&#xe001;',
			'icon24-calendar' : '&#xe003;',
			'icon24-bubble' : '&#xe004;',
			'icon24-lab' : '&#xe009;',
			'icon24-lanyrd' : '&#xe00b;',
			'icon24-twitter' : '&#xe00c;',
			'icon24-film' : '&#xe00e;',
			'icon24-location' : '&#xe00f;',
			'icon24-mail' : '&#xe013;',
			'icon24-microphone' : '&#xe014;',
			'icon24-graduate' : '&#xe016;',
			'icon24-coffee' : '&#xf0f4;',
			'icon24-attachment' : '&#xe01a;',
			'icon24-hammer' : '&#xf291;',
			'icon24-food' : '&#xe007;',
			'icon24-picture' : '&#xf03e;',
			'icon24-user' : '&#xe008;',
			'icon24-clock' : '&#xe002;',
			'icon24-bubbles' : '&#xe010;',
			'icon24-house' : '&#xe005;',
			'icon24-ticket' : '&#xe011;',
			'icon24-uniF47D' : '&#xf47d;',
			'icon24-tag' : '&#xf02b;',
			'icon24-pencil' : '&#xf040;',
			'icon24-tv' : '&#xe006;',
			'icon24-play-alt' : '&#xe00a;',
			'icon24-file-pdf' : '&#xe00d;',
			'icon24-attachment-2' : '&#xe012;',
			'icon24-presentation' : '&#xf0c4;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon24-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};