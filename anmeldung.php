<?php

### Konfiguration ###

# Bitte passen Sie die folgenden Werte an, bevor Sie das Script benutzen!

# An welche Adresse sollen die Mails gesendet werden?
$strEmpfaenger = 'iwm@uni-koblenz.de';

# Welche Adresse soll als Absender angegeben werden?
# (Manche Hoster lassen diese Angabe vor dem Versenden der Mail ueberschreiben)
$strFrom       = '"KeLT" <iwm@uni-koblenz.de>';

# Welchen Betreff sollen die Mails erhalten?
$strSubject    = 'Anmeldung - 13. Koblenzer eLearning-Tage 2015';

# Zu welcher Seite soll als "Danke-Seite" weitergeleitet werden?
# Wichtig: Sie muessen hier eine gueltige HTTP-Adresse angeben!
$strReturnhtml = 'confirm';

# Welche(s) Zeichen soll(en) zwischen dem Feldnamen und dem angegebenen Wert stehen?
$strDelimiter  = ":\t";

### Ende Konfiguration ###

if($_POST)
{
 $strMailtext = "";

 while(list($strName,$value) = each($_POST))
 {
  if(is_array($value))
  {
   foreach($value as $value_array)
   {
    $strMailtext .= $strName.$strDelimiter.$value_array."\n";
   }
  }
  else
  {
   $strMailtext .= $strName.$strDelimiter.$value."\n";
  }
 }

 if(get_magic_quotes_gpc())
 {
  $strMailtext = stripslashes($strMailtext);
 }

 mail($strEmpfaenger, $strSubject, $strMailtext, "From: ".$strFrom)
  or die("Die Mail konnte nicht versendet werden.");
 header("Location: $strReturnhtml");
 exit;
}

?>


<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>13. Koblenzer eLearning-Tage</title>


  <link rel="stylesheet" href="css/app.css" />

  <script src="js/vendor/custom.modernizr.js"></script>

</head>
<body>
	<!-- START Header -->
	<div id="header">
		<div class="header-bg">

			<!-- START Logo -->
			<div id="logo">
				<a href="index" title="zur Startseite">
					<h1>13. Koblenzer eLearning-Tage</h1>
				</a>
			</div>
			<!-- ENDE Logo -->

			<!-- START Hauptnavigation -->
			<div id="navigation">
				<nav class="top-bar">
					<ul class="title-area">
						<li class="name has-form">
							<h1></h1>
						</li>
						<li class="toggle-topbar menu-icon">
							<a href="#"><span>Menu</span></a>
						</li>
					</ul>
					<section class="top-bar-section">
						<ul class="right">
							<li class="divider"></li>
							<li><a href="index">Startseite</a></li>
							<li class="divider"></li>
							<li><a href="programm">Programm</a></li>
							<li class="divider"></li>
							<li class="active"><a href="anmeldung">Anmeldung</a></li>
							<li class="divider"></li>
							<li><a href="anfahrt">Anfahrt &amp; Unterkunft</a></li>
						</ul>
					</section>
				</nav>
			</div>
			<!-- ENDE Hauptnavigation -->
		</div>
	</div>
	<!-- ENDE Header -->

	<!-- START Contentbereich -->
	<div id="content">
		<!-- Seitentitel -->
		<div id="pagetitle">
			<h2>Anmeldeformular</h2>
		</div>

		<!-- Content -->
		<form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
				<!-- Name -->
				<div class="form-unit">
					<div class="form-input">
						<label>Name (Pflichtfeld)</label>
						<input type="text" name="Versender" required>
					</div>
				</div>
				<!-- Mail -->
				<div class="form-unit">
					<div class="form-input">
						<label>E-Mail (Pflichtfeld)</label>
						<input type="email" name="E-Mail" required>
					</div>
				</div>
				<!-- Text -->
				<div class="form-unit">
					<div class="form-input">
						<label>Ihre Nachricht</label>
						<textarea name="Nachricht"></textarea>
					</div>
				</div>
				<div class="form-unit">
					<div class="form-submit">
						<input class="register" type="submit" value="anmelden" />
					</div>
				</div>
		</form>

	</div>
	<!-- ENDE Contentbereich -->


	<!-- START Footer -->
	<div id="footer">
		<ul>
			<li><a href="kontakt">Kontakt</a></li>
			<li><a href="impressum">Impressum</a></li>
			<li><a href="datenschutz">Datenschutz</a></li>
		</ul>
		<ul>
			<li class="footer-logo"><a href="http://iwm.ko-ld.de" title="Institut für Wissensmedien Koblenz-Landau" target="_blank"><img src="img/iwm.png" alt="Logo IWM"></a></li>
			<li class="footer-logo"><a href="http://uni-koblenz-landau.de" title="Universität Koblenz-Landau" target="_blank"><img src="img/uni.png" alt="Logo Uni Koblenz"></a></li>
			<li class="footer-logo"><a href="http://www.hs-koblenz.de" title="Hochschule Koblenz" target="_blank"><img src="img/hs.png" alt="Logo HS Koblenz"></a></li>
			<li class="footer-logo"><a href="http://www.vcrp.de" title="Virtueller Campus Rheinland-Pfalz" target="_blank"><img src="img/vcrp.png" alt="Logo VCRP"></a></li>
		</ul>
	</div>
	<!-- ENDE Footer -->

  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
  '.js><\/script>')
  </script>

  <script src="js/foundation/foundation.js"></script>
	<script src="js/foundation/foundation.forms.js"></script>
	<script src="js/foundation/foundation.topbar.js"></script>


  <script>
    $(document).foundation();
  </script>

	<!-- Piwik -->
	<script type="text/javascript">
		var _paq = _paq || [];
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u=(("https:" == document.location.protocol) ? "https" : "http") + "://iwm.uni-koblenz.de/piwik/";
			_paq.push(['setTrackerUrl', u+'piwik.php']);
			_paq.push(['setSiteId', 1]);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
			g.defer=true; g.async=true; g.src=u+'piwik.js';
			s.parentNode.insertBefore(g,s);
		})();
	</script>
	<noscript><p><img src="http://iwm.uni-koblenz.de/piwik/piwik.php?idsite=1"
		style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->

</body>
</html>
